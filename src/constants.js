'use strict';
const dota2 = require('dota2');

// Change constants formats so it is more readable
const GameDelay = Object.keys(dota2.schema.LobbyDotaTVDelay).reduce((newObject, key) => {
	newObject[key.replace('LobbyDotaTV', 'DELAY')] = dota2.schema.LobbyDotaTVDelay[key];
	return newObject;
}, {});

const GameMode = Object.keys(dota2.schema.DOTA_GameMode).reduce((newObject, key) => {
	newObject[key.replace('DOTA_GAMEMODE_', '')] = dota2.schema.DOTA_GameMode[key];
	return newObject;
}, {});

const GameVersion = Object.keys(dota2.schema.DOTAGameVersion).reduce((newObject, key) => {
	newObject[key.replace('GAME_VERSION_', '')] = dota2.schema.DOTAGameVersion[key];
	return newObject;
}, {});

const PickPriority = {
	RANDOM: dota2.schema.DOTA_CM_PICK.DOTA_CM_RANDOM,
	RADIANT: dota2.schema.DOTA_CM_PICK.DOTA_CM_GOOD_GUYS,
	DIRE: dota2.schema.DOTA_CM_PICK.DOTA_CM_BAD_GUYS,
};

const SeriesType = dota2.SeriesType;
const ServerRegion = dota2.ServerRegion;

// Lobby options mapping for node-dota
const LobbyOptions = {
	name: 'game_name',
	region: 'server_region',
	mode: 'game_mode',
	seriesType: 'series_type',
	version: 'game_version',
	cheats: 'allow_cheats',
	fillWithBots: 'fill_with_bots',
	allowSpectators: 'allow_spectating',
	password: 'pass_key',
	firstPick: 'cm_pick',
	radiantWins: 'radiant_series_wins',
	direWins: 'dire_series_wins',
	allchat: 'allchat',
	leagueId: 'league_id',
	delay: 'dota_tv_delay'
};

module.exports = {
	Public: {
		GameDelay,
		GameMode,
		GameVersion,
		PickPriority,
		SeriesType,
		ServerRegion,
	},
	LobbyOptions
};
