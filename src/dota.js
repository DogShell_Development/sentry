'use strict';
const dota2 = require('dota2');
const Constants = require('./constants');
const Logger = require('./logger');
const Structures = require('./structures');
// const util = require('util');
const CronJob = require('cron').CronJob;
const EventEmitter = require('events');

const whitelist = Symbol('whitelist');
const teamReady = Symbol('teamReady');
const teamUnReady = Symbol('teamUnReady');
const startLobby = Symbol('startLobby');
const updateLobbyTeams = Symbol('updateLT');
const processLobbyMsg = Symbol('processLobbyMsg');

const eventEmitter = Symbol('eventEmitter');

/** Class representing a sentry dota 2 object. */
class SentryDota2 {
	/**
	 * Create the sentry dota 2 object
	 * @param {SentrySteam} sentrySteam the sentry steam object from which to open
	 */
	constructor(sentrySteam) {
		this.isReady = false;
		this.steamClient = sentrySteam.steamClient;
		this.client = new dota2.Dota2Client(this.steamClient);
		this.lobbyOptions;
		this.direTeam = [];
		this.radTeam = [];
		this.coaches = {
			radiant: [],
			dire: []
		};
		this.caster = [];
		this.spectator = [];
		this.joinedPlayers = [];
		this.radReady = false;
		this.direReady = false;
		this.logger = new Logger('Starting');
		this[eventEmitter] = new EventEmitter();
	}

	/**
	 * Open the dota 2 client and notices when it is ready
	 * @returns {Promise}
	 */
	launch() {
		this.client.launch();
		this.client.once('ready', () => {
			// this.client.leavePracticeLobby();
			this.leaveLobbyAll().then(() => this.initiated = true).catch(err => console.log(err));
			this.isReady = true;
			this.inLobby = false;
		});
		this.client.on('ready', () => this.isReady = true);
		this.client.on('unready', () => this.isReady = false);
		this.client.on('chatMessage', (channel, senderName, message) => {
			if (channel === 'Lobby_' + this.client.Lobby.lobby_id) {
				this[processLobbyMsg](senderName, message);
			}
		});
		this.client.on('partyInviteUpdate', (party) => {
			this.client.respondPartyInvite(party.group_id, false);
		});
		this.client.on('practiceLobbyUpdate', (lobby) => {
			if (lobby.game_state == 2) {
				if (this.isReady == false) {
					this.client.once('ready', () => this.client.abandonCurrentGame(() => { }));
				}
				else {
					this.client.abandonCurrentGame(() => { });
				}
			}

			if (this.lobbyOptions !== undefined && this.lobbyOptions.whitelist !== undefined)
				this[whitelist](lobby);

			this[updateLobbyTeams](lobby);
		});
	}

	[whitelist](lobby) {
		const userSnapshot = lobby.members.filter(obj => !(obj.id.low == lobby.leader_id.low));

		for (let member of userSnapshot) {
			// this.client.ToSteamID

			if (!isIn(member.id, this.lobbyOptions.whitelist, userSnapshot))
				this.client.practiceLobbyKick(this.client.ToAccountID(member.id.toString()), err => {
					if (err) {
						console.log(err);
					}

					this.logger.log('Kicked ' + member.name + ' ' + member.id.toString());
					this[eventEmitter].emit('playerKicked', member);
					// TODO: possibly some kind of reporting
				});
		}
	}

	/**
	 * Open the dota 2 client and notices when it is ready
	 * @param {Lobby} lobby object with lobby info
	 */
	async[updateLobbyTeams](lobby) {
		// Team id: 0
		this.radTeam = [];
		// Team id: 1
		this.direTeam = [];
		// Team id: 2
		this.caster = [];
		// Team id: 3 is used for coaches
		this.coaches = {
			radiant: [],
			dire: []
		};
		// Team id: 4
		this.spectator = [];

		const userSnapshot = lobby.members.filter(obj => !(obj.id.low == lobby.leader_id.low));

		for (let member of userSnapshot) {
			let user = this.lobbyOptions.whitelist.filter(d => { return d.id.toString() === member.id.toString(); })[0];

			// Radiant team
			if (member.team === 0) {
				if (user instanceof Structures.Caster || user instanceof Structures.Coach) {
					// await util.promisify(this.client.practiceLobbyKickFromTeam)(this.client.ToAccountID(member.id.toString()));
					await new Promise((resolve, reject) => this.client.practiceLobbyKickFromTeam(this.client.ToAccountID(member.id.toString()), (err, response) => err ? reject(err) : resolve(response)));
					continue;
				}
				this.radTeam.push(user);
			}
			// Dire team
			else if (member.team === 1) {
				if (user instanceof Structures.Caster || user instanceof Structures.Coach) {
					// await util.promisify(this.client.practiceLobbyKickFromTeam)(this.client.ToAccountID(member.id.toString()));
					await new Promise((resolve, reject) => this.client.practiceLobbyKickFromTeam(this.client.ToAccountID(member.id.toString()), (err, response) => err ? reject(err) : resolve(response)));
					continue;
				}
				this.direTeam.push(user);
			}
			// Caster
			else if (member.team == 2) {
				if (user instanceof Structures.Player || user instanceof Structures.Coach) {
					// await util.promisify(this.client.practiceLobbyKickFromTeam)(this.client.ToAccountID(member.id.toString()));
					await new Promise((resolve, reject) => this.client.practiceLobbyKickFromTeam(this.client.ToAccountID(member.id.toString()), (err, response) => err ? reject(err) : resolve(response)));
					continue;
				}
				this.caster.push(user);
			}
			// Coaches
			else if (member.team === 3) {
				if (user instanceof Structures.Player || user instanceof Structures.Caster) {
					// await util.promisify(this.client.practiceLobbyKickFromTeam)(this.client.ToAccountID(member.id.toString()));
					await new Promise((resolve, reject) => this.client.practiceLobbyKickFromTeam(this.client.ToAccountID(member.id.toString()), (err, response) => err ? reject(err) : resolve(response)));
					continue;
				}
				// member.coach_team === 0 --> Radiant coach
				// member.coach_team === 1 --> Dire coach
				if (member.coach_team === 0) {
					this.coaches.radiant.push(user);
				} else {
					this.coaches.dire.push(user);
				}
			}
			// Spectators
			else if (member.team === 4) {
				this.spectator.push(user);

				if (this.joinedPlayers.indexOf(user) === -1) {
					this.logger.log('Player: ' + member.name + ' joined the lobby');

					await timeout(500);

					this.joinedPlayers.push(user);
					if (this.client.chatChannels.filter(channel => channel.channel_name === 'Lobby_' + this.client.Lobby.lobby_id).length > 0) {
						this.msgLobby('Welcome to the match \'' + this.logger.name + '\' ' + member.name);
						this.msgLobby('For information on how to make changes to the lobby message "!help"');
					}
				}
			}
		}

		// TODO: team seeding

		// If a player leaves his team slot and said team was marked as ready --> team not ready
		if (this.radReady && this.radTeam.length < 5) {
			this.radReady = false;
			this.msgLobby('Radiant team is no longer ready');
		}
		if (this.direReady && this.direTeam.length < 5) {
			this.direReady = false;
			this.msgLobby('Dire team is no longer ready');
		}

		this[eventEmitter].emit('lobbySnapshot', {
			'radiant': this.radTeam,
			'dire': this.direTeam,
			'caster': this.caster,
			'coaches': this.coaches,
			'spectator': this.spectator
		});
	}

	/**
	 * Method used to process a lobby message and make the necesary changes
	 * @param {string} senderName username of sender
	 * @param {string} message Message to process
	 */
	[processLobbyMsg](senderName, message) {
		if (message === null || message === '' || message === undefined) {
			return;
		}
		this.logger.logMessage(senderName, message);
		this[eventEmitter].emit('lobbyMessage', { senderName: senderName, message: message });
		const splitMsg = message.toLowerCase().split(' ');
		const command = splitMsg[0];

		if (command == '!fp') {
			if (splitMsg[1] == 'radiant') {
				this.lobbyOptions.cm_pick = 1;
				this.updateLobby(this.lobbyOptions);
				this.msgLobby('First pick changed to radiant');
			}
			else if (splitMsg[1] == 'dire') {
				this.lobbyOptions.cm_pick = 2;
				this.updateLobby(this.lobbyOptions);
				this.msgLobby('First pick changed to dire');
			}
			else {
				this.msgLobby('First pick must either be "radiant" or "dire"');
			}
		} else if (command == '!help') {
			this.msgLobby('In order to use the lobby bot please enter one of the following commands:');
			this.msgLobby('!fp [dire/radient] : Switches the first pick to dire or radiant');
			this.msgLobby('!radscore [score] : Sets radients score to [score]');
			this.msgLobby('!direscore [score] : Sets dires score to [score]');
			this.msgLobby('!ready : Sets your team to ready');
			this.msgLobby('!unready : Sets your team to no longer ready');
			this.msgLobby('!start : Starts the match if both teams are ready');
			this.msgLobby('!region [regioncode]: Sets region according to this code/server list: https://github.com/Arcana/node-dota2#dota2serverregion--enum');
			this.msgLobby('!swap : Swap sides of both teams');
		} else if (command == '!radscore') {
			if (splitMsg.length != 1 && parseInt(splitMsg[1]) >= 0) {
				this.lobbyOptions.radiant_series_wins = parseInt(splitMsg[1]);
				this.updateLobby(this.lobbyOptions);
				this.msgLobby('Radiant series wins set to: ' + splitMsg[1]);
			}
			else {
				this.msgLobby('A score must be provided');
			}
		} else if (command == '!direscore') {
			if (splitMsg.length != 1 && parseInt(splitMsg[1]) >= 0) {
				this.lobbyOptions.dire_series_wins = parseInt(splitMsg[1]);
				this.updateLobby(this.lobbyOptions);
				this.msgLobby('Dire series wins set to: ' + splitMsg[1]);
			}
			else {
				this.msgLobby('A score must be provided');
			}
		} else if (command == '!start') {
			this[startLobby]();
		} else if (command == '!ready') {
			this[teamReady](senderName);

			if (this.direReady && this.radReady) {
				this.msgLobby('Both teams ready, message "!start" to begin');
			}
		} else if (command == '!unready') {
			this[teamUnReady](senderName);
		} else if (command == '!region') {
			if (splitMsg.length != 1 && parseInt(splitMsg[1]) >= 1) {
				this.lobbyOptions.server_region = parseInt(splitMsg[1]);
				this.updateLobby(this.lobbyOptions);
				this.msgLobby('Region to: ' + splitMsg[1]);
			}
			else {
				this.msgLobby('A region must be provided');
			}
		} else if (command == '!swap') {
			this.swapTeamsInLobby()
				.then(() => {
					this.msgLobby('Sides swapped');
				})
				.catch(() => {
					this.msgLobby('An error occurred while swapping sides');
				});
		}
		// else if (command == '!ll') {
		// 	this.leaveLobby();
		// }
	}

	[teamReady](senderName = '') {
		const userSnapshot = this.client.Lobby.members.filter(obj => !(obj.id.low == this.client.Lobby.leader_id.low));

		let dotaMember = userSnapshot.filter(user => { return user.name.toString() === senderName.toString(); })[0];
		let member = this.joinedPlayers.filter(mem => { return mem.id.toString() === dotaMember.id.toString(); })[0];

		if (this.radTeam.indexOf(member) != -1) {
			if (this.radTeam.length < 5) {
				this.radReady = false;
				this.msgLobby('Radiant team needs at least ' + (5 - this.radTeam.length) + ' more players to be ready');
			} else if ((this.lobbyOptions.team_banner_required ? member.teamID == undefined : false) || (member.teamID != undefined && correctBanner(member, this.client.Lobby.team_details, true))) {
				this.radReady = false;
				this.msgLobby('Radiant team needs to set its team banner.');
			}
			else {
				this.radReady = true;
				this.msgLobby('Radiant team ready');
			}
		}
		else if (this.direTeam.indexOf(member) != -1) {
			if (this.direTeam.length < 5) {
				this.direReady = false;
				this.msgLobby('Dire team needs at least ' + (5 - this.direTeam.length) + ' more players to be ready');
			} else if ((this.lobbyOptions.team_banner_required ? member.teamID == undefined : false) || (member.teamID != undefined && correctBanner(member, this.client.Lobby.team_details, false))) {
				this.direReady = false;
				this.msgLobby('Dire team needs to set its team banner.');
			}
			else {
				this.direReady = true;
				this.msgLobby('Dire team ready');
			}
		}

		this[eventEmitter].emit('readyTeam', {
			'radiant': this.radReady,
			'dire': this.direReady
		});
	}

	[teamUnReady](senderName = '') {
		const userSnapshot = this.client.Lobby.members.filter(obj => !(obj.id.low == this.client.Lobby.leader_id.low));

		let dotaMember = userSnapshot.filter(user => { return user.name.toString() === senderName.toString(); })[0];
		let member = this.joinedPlayers.filter(mem => { return mem.id.toString() === dotaMember.id.toString(); })[0];

		if (this.radTeam.indexOf(member) != -1) {
			this.radReady = false;
			this.msgLobby('Radiant team is no longer ready');
		}
		else if (this.direTeam.indexOf(member) != -1) {
			this.direReady = false;
			this.msgLobby('Dire team is no longer ready');
		}

		this[eventEmitter].emit('readyTeam', {
			'radiant': this.radReady,
			'dire': this.direReady
		});
	}

	async[startLobby]() {
		if (!this.radReady || this.radTeam.length < 5 || !correctBanner(this.radTeam[0], this.client.Lobby.team_id, true)) {
			return this.msgLobby('Radiant team: you must be ready and complete to begin');
		}
		if (!this.direReady || this.direTeam.length < 5 || !correctBanner(this.direTeam[0], this.client.Lobby.team_id, false)) {
			return this.msgLobby('Dire team: you must be ready and complete to begin');
		}

		let awaitCaster = false;
		let specs = this.spectator.filter(obj => !(obj.id.low == this.client.Lobby.leader_id.low));

		for (let member of specs) {
			if (member instanceof Structures.Player) {
				// await util.promisify(this.client.practiceLobbyKick)(this.client.ToAccountID(member.id.toString()));
				await new Promise((resolve, reject) => this.client.practiceLobbyKick(this.client.ToAccountID(member.id.toString()), (err, data) => err ? reject(err) : resolve(data)));
			}
			if (member instanceof Structures.Caster) {
				awaitCaster = true;
			}
		}
		// TODO: caster
		if (awaitCaster) {
			return this.msgLobby('Casters: you must be in a broadcaster slots');
		}

		this[eventEmitter].emit('lobbyLaunched');

		this.client.launchPracticeLobby();
	}

	/**
	 * Send a message to the current lobby.
	 * @param {string} message
	 */
	msgLobby(message) {
		if (this.client.Lobby) {
			this.client.sendMessage(message, 'Lobby_' + this.client.Lobby.lobby_id, 3);
		}
	}

	/**
	 * Get a player MMR from the profile card
	 * @param {string} playerId
	 * @returns {Promise} resolves to an object like { solo: <number>, party: <number> }
	 */
	getPlayerMMR(playerId) {
		// Get profile card information
		return new Promise((resolve, reject) => this.client.requestProfileCard(
			playerId,
			(err, data) => {
				if (err) {
					return reject(err);
				}
				// Filter by stat_id (1 for solo, 2 for party)
				// Map to get only the stat property
				// Reduce to an object like { solo: <number>, party: <number> }
				let mmr = data.slots.filter(
					slot => slot.stat && (slot.stat.stat_id === 1 || slot.stat.stat_id === 2))
					.map(slot => slot.stat)
					.reduce((obj, stat) => Object.assign(obj, {
						[stat.stat_id === 1 ? 'solo' : 'party']: stat.stat_score
					}), {});
				return resolve(mmr);
			}
		)
		);
	}

	/**
	 * Create a lobby and join the spectators
	 * @param {object} options
	 * @returns {Promise} resolves if the lobby was created correctly
	 */
	async createLobby(options) {
		// Convert options so node-dota understands
		this.lobbyOptions = Object.keys(options).reduce((newObject, key) => {
			let newKey = Constants.LobbyOptions[key] || key;
			newObject[newKey] = options[key];
			return newObject;
		}, {});

		for (let i = 0; i < 10 && !this.initiated; i += 1) {
			await timeout(100);
		}

		if (!this.initiated)
			await this.leaveLobbyAll();

		// Create the lobby
		// await util.promisify(this.client.createPracticeLobby)(this.lobbyOptions);
		await new Promise((resolve, reject) => this.client.createPracticeLobby(this.lobbyOptions, err => err ? reject(err) : resolve()));

		this[eventEmitter].removeAllListeners();
		// this[eventEmitter] = new EventEmitter();
		this[eventEmitter].emit('lobbyCreated');

		// Join the spectators
		this.inLobby = true;
		this.logger = new Logger('Lobby created for \'' + this.lobbyOptions.game_name + '\'');
		this.client.joinChat('Lobby_' + this.client.Lobby.lobby_id, 3);

		// Invite all players and caster to the lobby
		if (this.lobbyOptions.autoinvite === undefined || this.lobbyOptions.autoinvite) {
			if (this.lobbyOptions && this.lobbyOptions.whitelist) {
				for (let member of this.lobbyOptions.whitelist) {
					if (member instanceof Structures.Player || member instanceof Structures.Caster) {
						this.inviteToLobby(member.id);
					}
				}
			}
		}

		// when recreating a lobby, clear the old joined players list
		this.joinedPlayers = [];

		// start CRON job

		if (this.lobbyOptions['cron_repeats'] != undefined) {
			try {
				let c = new CronJob({
					cronTime: this.lobbyOptions['cron_pattern'],
					onTick: () => {
						this[eventEmitter].emit('cronTick');
						if (this.lobbyOptions['cron_repeats'] <= 0) {
							c.stop();
						}
						this.lobbyOptions['cron_repeats'] -= 1;
					},
					onComplete: () => {
						this[eventEmitter].emit('cronEnded');
					},
					start: true
				});
			} catch (ex) {
				// console.log('cron pattern not valid');
			}
		}

		// return await util.promisify(this.client.joinPracticeLobbyTeam)(1, 4);
		return await new Promise((resolve, reject) => this.client.joinPracticeLobbyTeam(1, 4, (err, data) => {
			return err ? reject(err) : resolve(Object.assign(this[eventEmitter], data));
		}));
	}

	/**
			* Invite a player to the lobby
			* @param {object} steamId
			* @returns {Promise} resolves if the player was added correctly
			*/
	async inviteToLobby(steam_id) {
		// Create the lobby
		if (this.inLobby) {
			this.client.inviteToLobby(steam_id);
			this[eventEmitter].emit('inviteToLobby', steam_id);
			return;
		} else {
			return Promise.reject('Not in a lobby');
		}
	}

	/**
	 * Update a lobby
	 * @param {object} options
	 * @returns {Promise} resolves if the lobby was updated correctly
	 */
	updateLobby(options) {
		this.lobbyOptions = Object.keys(options).reduce((newObject, key) => {
			let newKey = Constants.LobbyOptions[key] || key;
			newObject[newKey] = options[key];
			return newObject;
		}, {});
		return new Promise((resolve, reject) =>
			this.client.configPracticeLobby(this.client.Lobby.lobby_id, this.lobbyOptions, (err, data) => {
				if (err)
					return reject(err);

				this[eventEmitter].emit('lobbyUpdated');
				resolve(data);
			})
		);
	}

	/**
	 * Leave a lobby
	 * @returns {Promise} resolves if the client left correctly
	 */
	leaveLobby() {
		return new Promise((resolve, reject) => {
			this.client.leavePracticeLobby((err, data) => {
				if (err) {
					reject(err);
				} else {
					this.inLobby = false;
					this[eventEmitter].emit('leftLobby');
					resolve(data);
				}
			});
		});
	}

	/**
	 * Leave a lobby while kicking all the players inside
	 * @returns {Promise} resolves if the client left correctly
	 */
	async leaveLobbyAll() {
		if (!this.client.Lobby) {
			this.client.flipLobbyTeams();
			for (let i = 0; i < 10 && !this.client.Lobby; i += 1) {
				await timeout(100);
			}
		}

		if (this.client.Lobby && this.client.Lobby.members.length && (this.client.Lobby.members.length - 1)) {

			const userSnapshot = this.client.Lobby.members.filter(obj => !(obj.id.low == this.client.Lobby.leader_id.low));

			for (let lobbyMember of userSnapshot) {
				// await util.promisify(this.client.practiceLobbyKick)(this.client.ToAccountID(lobbyMember.id.toString()));
				await new Promise((resolve, reject) => this.client.practiceLobbyKick(this.client.ToAccountID(lobbyMember.id.toString()), (err) => err ? reject(err) : resolve()));
			}
		}

		return await this.leaveLobby();
	}

	/**
	 * Get lobby information
	 * @returns {Promise} resolves to an object representing the lobby
	 */
	async getLobbyInfo() {
		if (!this.client.Lobby) {
			throw new Error('Not inside of a lobby');
		} else {
			let lobby = this.client.Lobby;
			let availableOptions = Object.keys(Constants.LobbyOptions)
				.reduce((newObject, key) => {
					newObject[Constants.LobbyOptions[key]] = key;
					return newObject;
				}, {});
			let options = Object.keys(lobby).filter(key => Object.keys(availableOptions).indexOf(key) > -1)
				.reduce((newObject, key) => {
					newObject[availableOptions[key]] = lobby[key];
					return newObject;
				}, {});

			return {
				lobbyId: lobby.lobby_id,
				matchId: lobby.match_id,
				options
			};
		}
	}

	/*
	 * Swap the teams in a lobby
	 * @returns {Promise} resolves if the teams were swapped correctly
	 */
	swapTeamsInLobby() {
		return new Promise((resolve, reject) => this.client.flipLobbyTeams((err, data) => {
			if (err)
				return reject(err);

			this[eventEmitter].emit('lobbyTeamsSwapped');
			resolve(data);
		}));
	}

	/*
	 * Request Channel data
	 * @returns {Promise} resolves if the request processed.
	 */
	async requestChannelData() {
		return this.client.requestChatChannels();
	}
}

module.exports = SentryDota2;

const correctBanner = (member, team_details, radiant = true) => {
	let i = radiant ? 0 : 1;
	return member.teamID == undefined ? true : member.teamID.toString() !== (team_details[i] !== undefined ? team_details[i].team_id : -1).toString();
};

const timeout = (ms) => {
	return new Promise(resolve => setTimeout(resolve, ms));
};

const isIn = (id, memberObj, userSnapshot) => {
	let found = false;
	let member = null;

	for (member of memberObj) {
		if (id.toString() === member.id) {
			found = true;
			break;
		}
	}

	if (found && member instanceof Structures.Player && member.standinID !== null) {
		found = userSnapshot.filter(mem => mem.id.toString() === member.standinID.toString()).length <= 0;
	}

	return found;
};