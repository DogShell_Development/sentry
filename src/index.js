'use strict';
const SentrySteam = require('./steam');
const SentryDota = require('./dota');
const SentryConstants = require('./constants');
const SentryStructures = require('./structures');

/** Class representing a sentry object.
 *	wrapper for node-steam and node-dota
 */
class Sentry {
	/**
	 * Create the sentry object
	 * @param {Object} config object with the config params
	 * @param {Number} timeLimit Number of milliseconds to wait for steam and dota connection
	 * @param {Boolean} autoRelogin if sentry needs to relogin on error
	 */
	constructor(config, timeLimit = 30000, autoRelogin = true, autoReloginWaitTime = 5000) {
		this.userConfig = config.user;
		this.useTimeLimit = !!timeLimit;
		this.timeLimit = timeLimit;
		let onLogInResponse = () => {
			this.steam.setProfileInfo(this.userConfig.name, this.userConfig.status);
			this.dota = new SentryDota(this.steam);
			this.dota.launch();
		};
		let onErrorResponse = error => {
			let errorObj = {
				error: 'Steam has failed to start',
				code: -1,
				description: error
			};
			console.warn(`Dota Sentry error: ${JSON.stringify(errorObj)}`);
		};
		this.steam = new SentrySteam(autoRelogin, autoReloginWaitTime);
		this.steam.login(
			this.userConfig.username,
			this.userConfig.password,
			this.userConfig.guardCode,
			onLogInResponse,
			onErrorResponse
		);
	}

	getClient() {
		return this.steam.getClient();
	}

	/**
	 * Pass the lobby message along to the dota getClient
	 * @param {string} lobbyMessage
	 */
	// processLobbyMsg(message) {
	// 	this.dota.processLobbyMsg(message);
	// }

	/**
	 * If the dota client is ready
	 * @returns {boolean}
	 */
	isDotaReady() {
		return this.dota.isReady;
	}

	/**
	 * If the steam client is ready
	 * @returns {boolean}
	 */
	isSteamReady() {
		return this.steam.isReady;
	}

	/**
	 * Wait until the dota 2 client is ready
	 * timeLimit {number} timeLimit maximum time to wait before returning an error
	 * @returns {Promise} resolves if the client is ready, rejects if there is an error
	 */
	async waitUntilReady(timeLimit) {
		const timeout = (ms) => {
			return new Promise(resolve => setTimeout(resolve, ms));
		};

		let useTimeLimit = true;
		if (typeof timeLimit === 'undefined') {
			useTimeLimit = this.useTimeLimit;
			timeLimit = this.timeLimit;
		}

		// if time is up
		if (useTimeLimit && timeLimit <= 0) {
			let client = !this.isSteamReady() ? 'Steam' : 'Dota2';
			this.steam.autoRelogin = false;
			let error = {
				error: `${client} has taken to much time to start, try again later.`,
				code: -1
			};
			console.error(`Dota Sentry error: ${JSON.stringify(error)}`);
			return Promise.reject(error);
		}
		// while steam and dota are not ready
		if (!this.isSteamReady() || !this.isDotaReady()) {
			let next = useTimeLimit ? timeLimit - 100 : undefined;
			await timeout(100);
			return await this.waitUntilReady(next);
		}

		return;
	}

	/**
	 * Get a player MMR by player id
	 * @param {string} playerId
	 * @returns {Promise} resolves to an object like { solo: <number>, party: <number> }
	 */
	async getPlayerMMR(playerId) {
		await this.waitUntilReady();
		return this.dota.getPlayerMMR(playerId);
	}

	async sendMessageToLobby(msg) {
		try {
			await this.waitUntilReady();
			return await this.dota.msgLobby(msg);
		}
		catch (err) {
			return Promise.reject({
				error: 'Error trying to send message to the lobby, the client might be in a lobby or some options are not valid.',
				code: err
			});
		}
	}

	/**
	 * Create a lobby
	 * @param {object} options
	 * @returns {Promise} resolves if the lobby was created correctly
	 */
	async createLobby(options) {
		try {
			await this.waitUntilReady();
			return await this.dota.createLobby(options);
		}
		catch (err) {
			return Promise.reject({
				error: 'Error trying to create the lobby, the client might be in a lobby or some options are not valid.',
				code: err
			});
		}
	}

	async inviteToLobby(steam_id) {
		try {
			await this.waitUntilReady();
			return await this.dota.inviteToLobby(steam_id);
		}
		catch (err) {
			return Promise.reject({
				error: 'Error trying to invite to the lobby, the player might be in a lobby already.',
				code: err
			});
		}
	}

	/**
	 * Update a lobby
	 * @param {object} options
	 * @returns {Promise} resolves if the lobby was updated correctly
	 */
	async updateLobby(options) {
		try {
			await this.waitUntilReady();
			return await this.dota.updateLobby(options);
		}
		catch (err) {
			return Promise.reject({
				error: 'Error trying to edit the lobby, the client might not be a lobby or some options are not valid.',
				code: err
			});
		}
	}

	/**
	 * Leave a lobby
	 * @returns {Promise} resolves if the client left correctly
	 */
	async leaveLobby(options) {
		// return this.waitUntilReady().then(() => this.dota.leaveLobby(options))
		try {
			await this.waitUntilReady();
			return await this.dota.leaveLobby(options);
		} catch (err) {
			return Promise.reject({
				error: 'Error trying to leave the lobby, the client might not be in a lobby',
				code: err
			});
		}
	}

	/**
	 * Leave a lobby kicking all user inside
	 * @returns {Promise} resolves if the client left correctly
	 */
	async leaveLobbyAll(options) {
		// return this.waitUntilReady().then(() => this.dota.leaveLobby(options))
		try {
			await this.waitUntilReady();
			return await this.dota.leaveLobbyAll(options);
		} catch (err) {
			return Promise.reject({
				error: 'Error trying to leave the lobby, the client might not be in a lobby',
				code: err
			});
		}
	}

	/**
	 * Get lobby information
	 * @returns {Promise} resolves to an object representing the lobby
	 */
	async getLobbyInfo() {
		try {
			await this.waitUntilReady();
			return await this.dota.getLobbyInfo();
		} catch (err) {
			return Promise.reject({
				error: 'Error trying to get the lobby, the client might not be in a lobby',
				code: err
			});
		}
	}

	/**
	 * Get lobby information
	 * @returns {Promise} resolves to an object representing the lobby
	 */
	async requestChannelData() {
		try {
			await this.waitUntilReady();
			return await this.dota.requestChannelData();
		} catch (err) {
			return Promise.reject({
				error: 'Error trying to get the lobby, the client might not be in a lobby',
				code: err
			});
		}
	}

	/**
	 * Swap the teams in a lobby
	 * @returns {Promise} resolves if the teams were swapped correctly
	 */
	async swapTeamsInLobby() {
		try {
			await this.waitUntilReady();
			return await this.dota.swapTeamsInLobby();
		} catch (err) {
			return Promise.reject({
				error: 'Error trying swapping teams, the client might not be in a lobby',
				code: err
			});
		}
	}
}

module.exports = Object.assign({ Sentry }, SentryConstants.Public, { structures: SentryStructures });
