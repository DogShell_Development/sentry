class Logger {
	constructor(name) {
		this.name = name;

		console.log((new Date()).toGMTString() + ': ' + name);
	}

	log() {
		// TODO: Add some persistant logger in here
		let content = JSON.stringify(arguments);
		console.log((new Date()).toGMTString() + ': ' + content);
	}

	logMessage(senderName, message) {
		// TODO: Add some persistant logger in here
		console.log((new Date()).toGMTString() + ': ' + senderName, ' - ', message);
	}
}

module.exports = Logger;
