'use strict';
const crypto = require('crypto');
const fs = require('fs');
const steam = require('steam');

/** Class representing a sentry steam object. */
class SentrySteam {

	/**
	 * Create the sentry steam object
	 */
	constructor(autoRelogin = true, autoReloginWaitTime = 5000) {
		this.autoRelogin = autoRelogin;
		this.waitTime = autoReloginWaitTime;
		this.isReady = false;
		this.steamClient = new steam.SteamClient();
		this.steamUser = new steam.SteamUser(this.steamClient);
		this.steamFriends = new steam.SteamFriends(this.steamClient);

		// Update sentry file
		this.steamUser.on('updateMachineAuth', (sentry, callback) => {
			let hashedSentry = crypto.createHash('sha1').update(sentry.bytes).digest();
			fs.writeFileSync('sentry', hashedSentry);
			callback({
				sha_file: hashedSentry
			});
		});

		// Update servers file
		this.steamClient.on('servers',
			servers => fs.writeFile('servers', JSON.stringify(servers), err => {
				if (err)
					console.log(err);
			})
		);

		this.steamFriends.on('message', (steamRoomID, message, type) => {
			if (type == steam.EChatEntryType.ChatMsg) { // Regular chat message
				this.steamFriends.sendMessage(steamRoomID, 'Hello, this account is currently in use for hosting lobbies');
			}
		});
	}



	/**
	 * Login to the steam client
	 * @param {string} username steam username
	 * @param {string} password steam password
	 * @param {string} guardCode authorization code from steam
	 * @returns {Promise}
	 */
	login(username, password, guardCode, onLogInResponse = () => null, onErrorResponse = () => null) {
		// Object sent to the steam client
		let loginDetails = {
			account_name: username,
			password
		};

		// set guard code if it exists
		if (guardCode) {
			loginDetails.auth_code = guardCode;
		}

		// Read sentry file if it exists
		try {
			let sentry = fs.readFileSync('sentry');
			if (sentry.length) {
				loginDetails.sha_sentryfile = sentry;
			}
		} catch (e) {
			// Not an error, not logging for now
		}

		this.steamClient.connect();
		// Login, only passing authCode if it exists
		this.steamClient.on('connected', () => this.steamUser.logOn(loginDetails));
		this.steamClient.on('error', err => {
			this.isReady = false;
			this.steamClient.disconnect();
			onErrorResponse(err);
			if (this.autoRelogin && !this.steamClient.connected) {
				setTimeout(() => this.steamClient.connect(), this.waitTime);
			}
		});
		this.steamClient.on('logOnResponse',
			// Check if the response is what we expect
			response => {
				this.isReady = true;
				if (response.eresult === steam.EResult.OK) {
					onLogInResponse();
				} else {
					onErrorResponse(response.eresult);
				}
			}
		);
	}

	/**
	 * Change name and status of the current account
	 * @param {string} name
	 * @param {string} status needs to be one of the names from steam.EPersonaState
	 * @returns {Promise}
	 */
	setProfileInfo(name, status) {
		if (name) {
			this.steamFriends.setPersonaName(name);
		}
		if (status && steam.EPersonaState[status]) {
			this.steamFriends.setPersonaState(steam.EPersonaState[status]);
		}
	}
}

module.exports = SentrySteam;
