class Base {
	constructor(id) {
		this._id = id;
	}

	get id() {
		return '' + this._id;
	}
}

class Player extends Base {
	constructor(id, teamID = null, standin = null) {
		super(id);
		this._standin = standin;
		this._teamID = teamID;
	}

	get standinID() {
		return this._standin;
	}

	get teamID() {
		return this._teamID;
	}

	isStandin() {
		return this.standin != null;
	}

	stadingInFor(id) {
		return this.standin == id;
	}
}

class Caster extends Base {
}

class Coach extends Base {
}

class Admin extends Base {
}

module.exports = {
	Player: Player,
	Caster: Caster,
	Coach: Coach,
	Admin: Admin
};
