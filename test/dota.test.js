'use strict';
const proxyquire = require('proxyquire').noCallThru();
const tape = require('tape');
const sinon = require('sinon');

// Sandbox object to be able to clear stubs after the tests
let sandbox;
// Stubs to be used during tests
const dota = require('dota2');
const constants = {
	LobbyOptions: {
		option1: 'lobbyOption1',
		option2: 'lobbyOption2',
	}
};
let dotaClient;
let SentryDota;

/**
 * Set up stubs for all tests
 */
let setup = function () {
	sandbox = sinon.sandbox.create();
	// Create stubs and spies
	dotaClient = sinon.createStubInstance(dota.Dota2Client);
	sandbox.stub(dota, 'Dota2Client', () => dotaClient);
	// Proxy the subject
	SentryDota = proxyquire('../src/dota', {
		'dota2': dota,
		'./constants': constants
	});
};

/**
 * Clean up the stubs after the test has finished
 */
let restore = function () {
	sandbox.restore();
};

/**
 * Wrapper to call setup and restore between tests
 */
const test = function (description, fn) {
	tape(description, assert => {
		setup();
		fn(assert);
		restore();
	});
};

test('test Sentry Dota creation', function (assert) {
	assert.plan(3);
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	assert.equal(subject.steamClient, 'steam-client',
		'Sentry Steam client should match');
	assert.equal(subject.client, dotaClient, 'Client is the correct object');
	assert.ok(dota.Dota2Client.withArgs('steam-client').calledOnce, 'Steam client param should match');
});

test('test Sentry Dota launch success', function (assert) {
	assert.plan(4);
	// Set up stubs
	dotaClient.on.withArgs('ready').callsArg(1);
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.launch();
	assert.ok(dotaClient.on.withArgs('ready').calledOnce, 'Ready event called once.');
	assert.ok(dotaClient.on.withArgs('unready').calledOnce, 'Unready event called once');
	assert.ok(dotaClient.launch.calledOnce, 'Launch called once');
	assert.ok(subject.isReady, 'Dota is ready');
});

test('test Sentry Dota launch error', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.on.withArgs('unready').callsArg(1);
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.launch();
	assert.notOk(subject.isReady, 'Dota is not ready');
});

test('test Sentry Dota get player MMR success', function (assert) {
	assert.plan(2);
	const expected = {
		solo: 10,
		party: 20
	};
	const response = {
		slots: [{
			stat: {
				stat_id: 1,
				stat_score: 10
			}
		}, {
			stat: {
				stat_id: 3,
				stat_score: 30
			}
		}, {
			stat: {
				stat_id: 2,
				stat_score: 20
			}
		}, {
			stat: null
		}]
	};
	// Set up stubs
	dotaClient.requestProfileCard.callsArgWith(1, null, response);
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.getPlayerMMR('player-id').then(MMR => {
		assert.ok(dotaClient.requestProfileCard.withArgs('player-id').calledOnce,
			'Request profile card called once');
		assert.deepEqual(MMR, expected, 'MMR object should match the expected');
	});
});

test('test Sentry Dota get player MMR failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.requestProfileCard.callsArgWith(1, 'error');
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.getPlayerMMR('player-id').catch(error =>
		assert.equal(error, 'error', 'An error is expected')
	);
});

test('test Sentry Dota create lobby success', function (assert) {
	assert.plan(3);
	const options = {
		option1: 'value1',
		option2: 'value2',
		password: 'pass'
	};
	const expectedOptions = {
		lobbyOption1: 'value1',
		lobbyOption2: 'value2',
		password: 'pass'
	};
	// Set up stubs
	dotaClient.createPracticeLobby.callsArg(1);
	dotaClient.joinChat.returns(true);
	dotaClient.inviteToLobby.returns(true);
	dotaClient.flipLobbyTeams.returns(true);
	dotaClient.practiceLobbyKick.callsArg(1);
	dotaClient.leavePracticeLobby.callsArgWith(0, null, 'response');
	dotaClient.joinPracticeLobbyTeam.callsArgWith(2, null, 'response');
	dotaClient.Lobby = { lobby_id: 0, members: [] };
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.createLobby(options).then(() => {
		assert.pass('Create lobby ended successfully');
		assert.ok(
			dotaClient.createPracticeLobby.withArgs(expectedOptions).calledOnce,
			'Create practice lobby called once'
		);
		assert.ok(dotaClient.joinPracticeLobbyTeam.withArgs(1, 4).calledOnce,
			'Join practice lobby team called once');
	});
});

test('test Sentry Dota create lobby failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.createPracticeLobby.callsArgWith(1, 'error');
	dotaClient.flipLobbyTeams.returns(true);
	dotaClient.practiceLobbyKick.callsArg(1);
	dotaClient.leavePracticeLobby.callsArgWith(0, null, 'response');
	dotaClient.Lobby = { members: [] };
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.createLobby({}).catch(error =>
		assert.equal(error, 'error', 'An error is expected')
	);
});

test('test Sentry Dota create lobby join team failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.createPracticeLobby.callsArg(1);
	dotaClient.joinChat.returns(true);
	dotaClient.inviteToLobby.returns(true);
	dotaClient.flipLobbyTeams.returns(true);
	dotaClient.practiceLobbyKick.callsArg(1);
	dotaClient.leavePracticeLobby.callsArgWith(0, null, 'response');
	dotaClient.Lobby = { lobby_id: 0, members: [] };

	dotaClient.joinPracticeLobbyTeam.callsArgWith(2, 'error');
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.createLobby({}).catch(error =>
		assert.equal(error, 'error', 'An error is expected')
	);
});

test('test Sentry Dota leave lobby success', function (assert) {
	assert.plan(2);
	// Set up stubs
	dotaClient.leavePracticeLobby.callsArgWith(0, null);
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.leaveLobby().then(() => {
		assert.pass('Create lobby ended successfully');
		assert.ok(dotaClient.leavePracticeLobby.calledOnce, 'Leave practice lobby called once');
	});
});

test('test Sentry Dota leave lobby failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.leavePracticeLobby.callsArgWith(0, 'error');
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.leaveLobby().catch(error =>
		assert.equal(error, 'error', 'An error is expected')
	);
});

test('test Sentry Dota get lobby info success', function (assert) {
	assert.plan(1);
	const expectedOptions = {
		lobbyId: 'lobby-id',
		matchId: 'match-id',
		options: {
			option1: 'value1',
			option2: 'value2'
		}
	};
	// Set up stubs
	dotaClient.Lobby = {
		lobby_id: 'lobby-id',
		match_id: 'match-id',
		lobbyOption1: 'value1',
		lobbyOption2: 'value2',
		lobbyOption3: 'value3'
	};
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.getLobbyInfo().then(lobbyInfo =>
		assert.deepEqual(lobbyInfo, expectedOptions, 'Lobby info should match')
	);
});

test('test Sentry Dota get lobby info failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.Lobby = null;
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.getLobbyInfo().catch(() =>
		assert.pass('An error is expected')
	);
});

test('test Sentry Dota update lobby success', function (assert) {
	assert.plan(2);
	const options = {
		option1: 'value1',
		option2: 'value2',
		password: 'pass'
	};
	const expectedOptions = {
		lobbyOption1: 'value1',
		lobbyOption2: 'value2',
		password: 'pass'
	};
	// Set up stubs
	dotaClient.Lobby = { lobby_id: 'lobby-id' };
	dotaClient.configPracticeLobby.callsArg(2);
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.updateLobby(options).then(() => {
		assert.pass('Update lobby ended successfully');
		assert.ok(
			dotaClient.configPracticeLobby.withArgs('lobby-id', expectedOptions).calledOnce,
			'Config practice lobby was called once'
		);
	});
});

test('test Sentry Dota update lobby failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.Lobby = { lobby_id: 'lobby-id' };
	dotaClient.configPracticeLobby.callsArgWith(2, 'error');
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.updateLobby({}).catch(error =>
		assert.equal(error, 'error', 'An error is expected')
	);
});

test('test Sentry Dota update lobby failure no lobby', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.Lobby = null;
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.updateLobby({}).catch(() =>
		assert.pass('An error is expected')
	);
});

test('test Sentry Dota swap teams in lobby success', function (assert) {
	assert.plan(2);
	// Set up stubs
	dotaClient.flipLobbyTeams.callsArg(0);
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.swapTeamsInLobby().then(() => {
		assert.pass('Swap teams in lobby ended successfully');
		assert.ok(dotaClient.flipLobbyTeams.calledOnce, 'Flip lobby teams called once');
	});
});

test('test Sentry Dota swap teams in lobby failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	dotaClient.flipLobbyTeams.callsArgWith(0, 'error');
	// Test
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.swapTeamsInLobby().catch(error =>
		assert.equal(error, 'error', 'An error is expected')
	);
});
