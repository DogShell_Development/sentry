'use strict';
const proxyquire = require('proxyquire').noCallThru();
const tape = require('tape');
const sinon = require('sinon');

// Sandbox object to be able to clear stubs after the tests
let sandbox;
// Stubs to be used during tests
const testConfig = {
	user: {
		username: 'username1',
		password: 'password1',
		guardCode: 'code1',
		name: 'name1',
		status: 'Busy'
	}
};
const required = {
	dota: require('../src/dota'),
	steam: require('../src/steam')
};
let sentryDota;
let sentrySteam;
let MainModule;

/**
 * Set up stubs for all tests
 */
let setup = function () {
	sandbox = sinon.sandbox.create();
	// Create stubs and spies
	sentryDota = sinon.createStubInstance(required.dota);
	sentrySteam = sinon.createStubInstance(required.steam);
	sandbox.stub(required, 'dota', () => sentryDota);
	sandbox.stub(required, 'steam', () => sentrySteam);
	sentrySteam.login.callsArg(3);
	// Proxy the subject
	MainModule = proxyquire('../src/index', {
		'./steam': required.steam,
		'./dota': required.dota
	});
};

/**
 * Clean up the stubs after the test has finished
 */
let restore = function () {
	sandbox.restore();
};

/**
 * Wrapper to call setup and restore between tests
 */
const test = function (description, fn) {
	tape(description, assert => {
		setup();
		fn(assert);
		restore();
	});
};

test('test Sentry module creation', function (assert) {
	assert.plan(4);
	// Test
	let subject = new MainModule.Sentry(testConfig, 1000, false);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	assert.deepEqual(subject.userConfig, testConfig.user,
		'Configurations should match');
	subject.waitUntilReady().then(() => {
		assert.ok(
			sentrySteam.login.withArgs('username1', 'password1', 'code1').calledOnce,
			'Steam login called correctly');
		assert.ok(
			sentrySteam.setProfileInfo.withArgs('name1', 'Busy').calledOnce,
			'Steam set profile info called correctly');
		assert.ok(sentryDota.launch.calledOnce, 'Dota launch called correctly');
	});
});

test('test Sentry get player MMR', function (assert) {
	assert.plan(3);
	const expected = {
		solo: 1234,
		party: 1324
	};
	// Set up stubs
	sentryDota.getPlayerMMR.returns(Promise.resolve(expected));
	// Test
	let playerId = 'player-id';
	let subject = new MainModule.Sentry(testConfig);
	subject.waitUntilReady = (limit) => {
		assert.notOk(limit, 'waitUntilReady should be called with empty params');
		return Promise.resolve();
	};
	subject.getPlayerMMR(playerId).then(response => {
		assert.equal(response, expected,
			'Response should match resolved value from SentryDota');
		assert.ok(sentryDota.getPlayerMMR.withArgs('player-id').calledOnce,
			'Dota get player MMR called correctly');
	});
});

test('test Sentry waitUntilReady both clients ready', function (assert) {
	assert.plan(1);
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.waitUntilReady().then(() =>
		assert.pass('waitUntilReady resolved successflly')
	);
});

test('test Sentry waitUntilReady steam not ready', function (assert) {
	assert.plan(1);
	// Set up stubs
	// sentrySteam.login.returns(Promise.reject());
	sentrySteam.login.returns();
	// Test
	let subject = new MainModule.Sentry(testConfig, 100);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => false;
	subject.waitUntilReady().catch(() => assert.pass('waitUntilReady failed to resolve'));
});

test('test Sentry waitUntilReady dota not ready', function (assert) {
	assert.plan(1);
	// Set up stubs
	// sentryDota.launch.returns(Promise.reject());
	sentryDota.launch.returns();
	// Test
	let subject = new MainModule.Sentry(testConfig, 100);
	subject.isDotaReady = () => false;
	subject.isSteamReady = () => true;
	subject.waitUntilReady().catch(() => assert.pass('waitUntilReady failed to resolve'));
});

test('test Sentry waitUntilReady time limit', function (assert) {
	assert.plan(1);
	// Set up stubs
	sentrySteam.login.returns(new Promise(resolve => setTimeout(resolve, 2000)));
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.waitUntilReady(1000).catch(() => assert.pass('waitUntilReady failed to resolve'));
});

test('test Sentry createLobby success', function (assert) {
	assert.plan(2);
	// Set up stubs
	sentryDota.createLobby.returns();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.createLobby('custom-options').then(() => {
		assert.pass('Create lobby worked as expected');
		assert.ok(sentryDota.createLobby.withArgs('custom-options').calledOnce,
			'Create lobby called correctly');
	});
});

test('test Sentry createLobby failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	sentryDota.createLobby.throws();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.createLobby('custom-options').catch(() =>
		assert.pass('An error is expected')
	);
});

test('test Sentry leaveLobby success', function (assert) {
	assert.plan(2);
	// Set up stubs
	sentryDota.leaveLobby.returns();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.leaveLobby().then(() => {
		assert.pass('Leave lobby worked as expected');
		assert.ok(sentryDota.leaveLobby.calledOnce,
			'Leave lobby called correctly');
	});
});

test('test Sentry leaveLobby failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	sentryDota.leaveLobby.throws();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.leaveLobby().catch(() =>
		assert.pass('An error is expected')
	);
});

test('test Sentry updateLobby success', function (assert) {
	assert.plan(2);
	// Set up stubs
	sentryDota.updateLobby.returns();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.updateLobby('custom-options').then(() => {
		assert.pass('Update lobby worked as expected');
		assert.ok(sentryDota.updateLobby.withArgs('custom-options').calledOnce,
			'Update lobby called correctly');
	});
});

test('test Sentry updateLobby failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	sentryDota.updateLobby.throws();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.updateLobby('custom-options').catch(() =>
		assert.pass('An error is expected')
	);
});

test('test Sentry get Lobby Info success', function (assert) {
	assert.plan(2);
	// Set up stubs
	sentryDota.getLobbyInfo.returns(Promise.resolve('lobby-info'));
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.getLobbyInfo().then(info => {
		assert.equal(info, 'lobby-info', 'Get lobby info worked as expected');
		assert.ok(sentryDota.getLobbyInfo.calledOnce,
			'Get lobby info called correctly');
	});
});

test('test Sentry get Lobby Info failure', function (assert) {
	assert.plan(1);
	// Set up stubs
	sentryDota.getLobbyInfo.throws();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.getLobbyInfo().catch(() =>
		assert.pass('An error is expected')
	);
});

test('test Sentry swap teams in lobby', function (assert) {
	assert.plan(2);
	// Set up stubs
	sentryDota.swapTeamsInLobby.returns();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.swapTeamsInLobby().then(() => {
		assert.pass('Swap teams in lobby worked as expected');
		assert.ok(sentryDota.swapTeamsInLobby.calledOnce,
			'Swap teams in lobby called correctly');
	});
});

test('test Sentry swap teams in lobby', function (assert) {
	assert.plan(1);
	// Set up stubs
	sentryDota.swapTeamsInLobby.throws();
	// Test
	let subject = new MainModule.Sentry(testConfig);
	subject.isDotaReady = () => true;
	subject.isSteamReady = () => true;
	subject.swapTeamsInLobby().catch(() =>
		assert.pass('An error is expected')
	);
});
