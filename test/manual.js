var SentryModule = require('../src/index');

var sentry = new SentryModule.Sentry({
	user: {
		status: 'Busy'
	}
});

sentry.createLobby({
	'game_name': 'EL test lobby',
	'server_region': SentryModule.ServerRegion.EUROPE,
	'game_mode': SentryModule.GameMode.DOTA_GAMEMODE_CM,
	'allchat': false,
	'whitelist': [
		// new SentryModule.structures.Player('76561198012867054', '2484862')
		// new SentryModule.structures.Coach('76561198012867054')
		// new SentryModule.structures.Player('76561198012867054')
		// new SentryModule.structures.Caster('76561198012867054')
		new SentryModule.structures.Admin('76561198012867054')
	],
	// 'autoinvite': false,
	// 'team_banner_required': true
	'cron_pattern': '0 */5 * * * *',
	'cron_repeats': 5
})
	.then((events) => {
		// events emits: lobbyCreated; inviteToLobby; lobbyUpdated; leftLobby; lobbyTeamsSwapped, playerKicked (member); lobbySnapshot ({radiant: [], dire: [], caster: [], coaches: [], spectator: []}); lobbyMessage ({senderName: "", message: ""}); readyTeam ({'radiant': Bool, 'dire': Bool}); lobbyLaunched;  
		// .then(() => {
		console.log('Lobby created');

		events.on('lobbyMessage', (msg) => console.log(msg));

		// let counter = 10;

		// events.on('cronTick', () => {
		// 	counter -= 1;
		// 	sentry.sendMessageToLobby('You have ' + counter * 5 + ' seconds left until the lobby closes itself.');
		// });

		// events.on('cronEnded', () => {
		// 	sentry.sendMessageToLobby('Closing lobby');
		// 	sentry.leaveLobbyAll();
		// });
	})
	.catch((err) => {
		console.error(err);
	});