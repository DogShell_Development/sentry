'use strict';
const proxyquire = require('proxyquire').noCallThru();
const tape = require('tape');
const sinon = require('sinon');

// Sandbox object to be able to clear stubs after the tests
let sandbox;
// Stubs to be used during tests
const steam = require('steam');
let steamClient;
let steamFriends;
let steamUser;
let SentrySteam;

/**
 * Set up stubs for all tests
 */
let setup = function () {
	sandbox = sinon.sandbox.create();
	// Create stubs and spies
	steamClient = sinon.createStubInstance(steam.SteamClient);
	steamFriends = sinon.createStubInstance(steam.SteamFriends);
	steamUser = sinon.createStubInstance(steam.SteamUser);
	sandbox.stub(steam, 'SteamClient', () => steamClient);
	sandbox.stub(steam, 'SteamFriends', () => steamFriends);
	sandbox.stub(steam, 'SteamUser', () => steamUser);
	// Proxy the subject
	SentrySteam = proxyquire('../src/steam', {
		'steam': steam,
		'fs': { readFileSync: () => {} }
	});
};

/**
 * Clean up the stubs after the test has finished
 */
let restore = function () {
	sandbox.restore();
};

/**
 * Wrapper to call setup and restore between tests
 */
const test = function(description, fn) {
	tape(description, assert => {
		setup();
		fn(assert);
		restore();
	});
};

test('test Sentry Steam creation', function(assert) {
	assert.plan(5);
	// Test
	let subject = new SentrySteam();
	assert.equal(subject.steamClient, steamClient,
		'Client is the correct object');
	assert.equal(subject.steamFriends, steamFriends,
		'Friends is the correct object');
	assert.equal(subject.steamUser, steamUser, 'User is the correct object');
	assert.ok(steamClient.on.withArgs('servers').calledOnce,
		'Servers event was set');
	assert.ok(steamUser.on.withArgs('updateMachineAuth').calledOnce,
		'Servers event was set');
});

test('test Sentry Steam login success', function(assert) {
	assert.plan(3);
	const expectedDetails = {
		account_name: 'username1',
		password: 'password1',
		auth_code: 'code1'
	};
	const logOnResponse = {
		eresult: 1
	};
	// Set up stubs
	steam.EResult = { OK: 1 };
	steamClient.on.withArgs('connected').callsArg(1);
	steamClient.on.withArgs('logOnResponse').callsArgWith(1, logOnResponse);
	// Test
	let onLogInResponse = () => {
		assert.pass('Should return success');
		assert.ok(steamClient.connect.calledOnce, 'Connect was called once');
		assert.ok(steamUser.logOn.withArgs(expectedDetails).calledOnce,
			'Log on called with correct params');
	};
	let subject = new SentrySteam();

	subject.onErrorResponse = () => assert.fail('There should be no error');
	subject.login('username1', 'password1', 'code1', onLogInResponse);
});

test('test Sentry Steam login error', function(assert) {
	assert.plan(1);
	// Set up stubs
	steamClient.on.withArgs('error').callsArg(1);
	// Test
	let onLogInResponse = () => assert.fail('Log in should not be performed');
	let onErrorResponse = () => assert.pass('An error is expected');
	let subject = new SentrySteam(false);
	subject.login('username1', 'password1', '', onLogInResponse, onErrorResponse);
});

test('test Sentry Steam login error with auto relogin', function(assert) {
	// Set up stubs
	steamClient.on.withArgs('error').callsArg(1);
	// Test
	let onLogInResponse = () => assert.fail('Log in should not be performed');
	let onErrorResponse = () => assert.pass('An error is expected');
	let subject = new SentrySteam(true, 50);
	subject.login('username1', 'password1', '', onLogInResponse, onErrorResponse);
	setTimeout(() => {
		assert.ok(steamClient.disconnect.calledOnce, 'The disconnect method should be called');
		assert.ok(steamClient.connect.calledTwice, 'The connect method should be called twice');
		assert.end();
	}, 100);
});

test('test Sentry Steam login bad response', function(assert) {
	assert.plan(1);
	const logOnResponse = {
		eresult: 2
	};
	// Set up stubs
	steam.EResult = { OK: 1 };
	steamClient.on.withArgs('connected').callsArg(1);
	steamClient.on.withArgs('logOnResponse').callsArgWith(1, logOnResponse);
	// Test
	let onLogInResponse = () => assert.fail('Log in should not be performed');
	let onErrorResponse = () => assert.pass('An error is expected');
	let subject = new SentrySteam(false);
	subject.login('username1', 'password1', '', onLogInResponse, onErrorResponse);
});

test('test Sentry Steam set profile info', function(assert) {
	assert.plan(2);
	// Set up stubs
	steam.EPersonaState = { Busy: 'busy-status' };
	// Test
	let subject = new SentrySteam();
	subject.setProfileInfo('name1', 'Busy');
	assert.ok(steamFriends.setPersonaName.withArgs('name1').calledOnce,
		'Set persona name called correctly');
	assert.ok(steamFriends.setPersonaState.withArgs('busy-status').calledOnce,
		'Set persona state called correctly');
});

test('test Sentry Steam set profile info no name', function(assert) {
	assert.plan(2);
	// Set up stubs
	steam.EPersonaState = { Busy: 'busy-status' };
	// Test
	let subject = new SentrySteam();
	subject.setProfileInfo(null, 'Busy');
	assert.equal(steamFriends.setPersonaName.callCount, 0,
		'Set persona name should not be called');
	assert.ok(steamFriends.setPersonaState.withArgs('busy-status').calledOnce,
		'Set persona state called correctly');
});

test('test Sentry Steam set profile info no status', function(assert) {
	assert.plan(2);
	// Test
	let subject = new SentrySteam();
	subject.setProfileInfo('name1', null);
	assert.ok(steamFriends.setPersonaName.withArgs('name1').calledOnce,
		'Set persona name called correctly');
	assert.equal(steamFriends.setPersonaState.callCount, 0,
		'Set persona state should not be called');
});

test('test Sentry Steam set profile info wrong status', function(assert) {
	assert.plan(2);
	// Set up stubs
	steam.EPersonaState = {};
	// Test
	let subject = new SentrySteam();
	subject.setProfileInfo('name1', 'busy-status');
	assert.ok(steamFriends.setPersonaName.withArgs('name1').calledOnce,
		'Set persona name called correctly');
	assert.equal(steamFriends.setPersonaState.callCount, 0,
		'Set persona state should not be called');
});
